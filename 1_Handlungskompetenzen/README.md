# Handlungskompetenzen

## Modulspezifische Handlungskompetenzen

 * A1 Unternehmens- und Führungsprozesse gestalten und verantworten
   * A1.6 Arbeitspsychologische Grundsätze im Umgang mit Mitarbeiterinnen und Mitarbeitern berücksichtigen sowie sozial und verantwortungsvoll handeln (Niveau: 3)
   * A1.7 Zusammenarbeit im Team gestalten, reflektieren und Regeln vereinbaren
   * A1.9 Interpersonelle Konflikte und schwierige individuelle Situationen erkennen, ansprechen und konstruktiv an Lösungen mitarbeiten
   * A1.10 Die Kommunikation und Zusammenarbeit unter Berücksichtigung relevanter Genderfragen, der Diversität und interkulturellen Gegebenheiten gestalten
   * A1.11 Die Motivation im Team fördern und dieses zu Höchstleistungen befähigen
   * A1.12 Kundenbeziehungen gestalten


 * A3 Die persönliche Entwicklung reflektieren und aktiv gestalten (Niveau: 3)
   * A3.1 Die eigenen Kompetenzen bezüglich der beruflichen Anforderungen regelmässig reflektieren, bewerten und daraus den Lernbedarf ermitteln
   * A3.2 Neues Wissen mit geeigneten Methoden erschliessen und arbeitsplatznahe Weiterbildung realisieren
   * A3.3 Neue Technologien kritisch reflexiv beurteilen, adaptieren und integrieren
   * A3.4 Die eigenen digitalen Kompetenzen kontinuierlich weiterentwickeln
   * A3.5 Das eigene Denken, Fühlen und Handeln reflektieren und geeignete persönliche Entwicklungsmassnahmen definieren und umsetzen 

## Anforderungsniveau

Das Anforderungsniveau einer Kompetenz ist durch die Komplexität der zu lösenden Problemstellung, die Veränderlichkeit und Unvorhersehbarkeit des Arbeitskontextes und die Verantwortlichkeit im Bereich der Zusammenarbeit und Führung definiert. HF Absolvierende sind generell in der Lage Problemstellungen und Herausforderungen zu analysieren, diese adäquat zu bewerten und mit innovativen Problemlösestrategien zu lösen. Die Handlungskompetenzen werden in vier Anforderungsniveaus eingestuft.

### Kompetenzniveau 1: Novizenkompetenz

Erfüllen selbständig fachliche Anforderungen; mehrheitlich wiederkehrende Aufgaben in einem überschaubaren und stabil strukturierten Tätigkeitsgebiet; Arbeit im Team und unter Anleitung.

### Kompetenzniveau 2: fortgeschrittene Kompetenz

Erkennen und analysieren umfassende fachliche Aufgabenstellungen in einem komplexen Arbeitskontext und sich veränderndem Arbeitsbereich; führen teils kleinere Teams; erledigen die Arbeiten selbständig unter Verantwortung einer Drittperson.

### Kompetenzniveau 3: Kompetenz professionellen Handelns

Bearbeiten neue komplexe Aufgaben und Problemstellungen in einem nicht vorhersehbaren Arbeitskontext; übernehmen die operative Verantwortung und planen, handeln und evaluieren autonom.

### Kompetenzniveau 4: Kompetenzexpertise

Entwickeln innovative Lösungen in einem komplexen Tätigkeitsfeld; antizipieren Veränderungen in der Zukunft und handeln proaktiv; übernehmen strategische Verantwortung und treiben Veränderungen und Entwicklungen voran.
