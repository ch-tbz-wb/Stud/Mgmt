# Umsetzung

 - Bereich: Die persönliche Entwicklung reflektieren und aktiv gestalten
 - Semester: 1

## Lektionen: 

* Präsenzunterricht: 40
* Fernunterricht: 0
* Selbststudium: 20

## Lernziele
 - **Persönlichkeit**
   - Ich verstehe mein Arbeitsverhalten und entdecke, wie es auf andere wirkt.
   - Ich lerne mein Potenzial dort einzusetzen, wo ich am erfolgreichsten sein kann.
   - Ich lerne, die Verhaltensweisen bei mir und anderen besser einzuschätzen.
   - Ich lerne, wie ich mein Umfeld positiv beeinflussen kann.
   - Ich entwickle Strategien, um die Zusammenarbeit zu intensivieren und die Produktivität zu erhöhen.
 - **Teamtraining**
   - Im Teamtraining wirst du systematisch die wichtigsten Erkenntnisse zur Teamarbeit kennenlernen und schrittweise einsetzen.
   - Im Einzelnen lernst du o die Erfolgsfaktoren in der Teamarbeit kennen und anwenden.
     - gemeinsam mit deinen Teamkollegen anhand eines Team-Benchmarks systematisch die Leistungsfähigkeit deines Teams zu erhöhen.
     - deine eigenen Stärken zu erkennen und dich in die Lage zu versetzen, diese auch angemessen in der Zusammenarbeit einzusetzen.
     - lösungsorientiertes Feedback zu erhalten und zu geben.
     - die Zusammenarbeit mit deinen Kollegen auf eine gemeinsame Leistung auszurichten.
 - **Resilienz**
   - Wie es mit deiner inneren Widerstandskraft aussieht
   - Wie resilient du bist
   - Welche Potenziale in dir schlummern
   - Wie du deine Kompetenz in den zehn Resilienzfaktoren verbessern kannst 
 - **Stressverhalten**
   - dich der eigenen Stressfaktoren und der Stressfaktoren deiner Mitarbeiter bewusster zu werden
   - das eigene Stressverhalten zu reflektieren
   - stressbedingte Verhaltensweisen deiner Mitarbeiter zu erkennen
   - Ressourcen im Umgang mit Stress zu erkennen und gezielt zu mobilisieren
   - die Einflussmöglichkeiten auf das eigene Stresserleben und das der Mitarbeiter zu erkennen

## Voraussetzungen

Keine

## Technik


## Methoden

Selbstreflexionsübungen, Teamprojekte, Feedbackrunden, Resilienztraining, Austausch

## Schlüsselbegriffe: 

## Lehr- und Lernformen: 

Lehrervorträge, Lehrgespräche, Workshop, Einzel- und Gruppenarbeiten, Lernfragen, Präsentationen, Austausch

## Lehrmittel:  

Repository



