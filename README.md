![TBZ Logo](./x_gitressourcen/tbz_logo.png) 

# PE - Führung, Kommunikation, Persönlichkeitsentwicklung

## Kurzbeschreibung des Moduls 

Das Modul hilft dir, deine Persönlichkeit zu verstehen und im Berufsalltag anzuwenden.

Weiter bietet das Modul umfassende Kompetenzen in den Bereichen effektive Kommunikation, Führungsfähigkeiten und Selbstentwicklung. Teilnehmer lernen, wie sie ihre Kommunikation sowohl mündlich als auch schriftlich sachlogisch, transparent und klar gestalten, um das Interesse der Adressaten zu gewinnen und überzeugend zu wirken. Es werden Methoden vermittelt, mit denen Informationen adressatengerecht selektiert und präsentiert werden können, unter Einsatz von geeigneten medialen und rhetorischen Elementen sowie Informations- und Kommunikationstechnologien. Darüber hinaus fördert das Modul die persönliche Entwicklung durch regelmäßige Reflexion der eigenen Kompetenzen, den Erwerb von neuem Wissen und die Integration von neuen Technologien, um so die beruflichen Anforderungen effektiv zu erfüllen und die eigene digitale Kompetenz kontinuierlich zu verbessern.

## Angaben zum Transfer der erworbenen Kompetenzen 

Die Kompetenzvermittlung erfolgt durch einen interaktiven Ansatz, der Selbstreflexion, Teamübungen und Feedback einschließt. Lehrpersonen fördern das Verständnis für eigenes Verhalten, den Einsatz persönlicher Stärken, die Einschätzung von Verhaltensweisen und die Entwicklung von Strategien zur Produktivitätssteigerung. Durch gezieltes Teamtraining und Resilienzschulung werden Lernende angeleitet, ihre Teamleistung zu verbessern und effektiv Herausforderungen zu meistern.

## Unterlagen zum Modul

### Organisatorisches

[Organisatorisches ](0_Organisatorisches)zur Autorenschaft dieser Dokumente zum Modul

## Handlungskompetenzen

[Handlungskompetenzen](1_Handlungskompetenzen) zu den Handlungszielen 

### Unterrichtsressourcen

[Unterrichtsressourcen](2_Unterrichtsressourcen) von Aufträgen und Inhalten zu den einzelnen Kompetenzen

### Umsetzung

[Umsetzung](3_Umsetzung) 

### Fragekatalog

[Fragekatalog ](4_Fragekatalog) Allgemein

### Handlungssituationen

[Handlungssituationen ](5_Handlungssituationen)mit möglichen Praxissituationen zu den einzelnen Handlungszielen

### Zertifizierungen 

Mögliche [Zertifizierungen](8_Zertifizierungen) für dieses Modul 

- - - 

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/ch/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/3.0/ch/88x31.png" /></a><br />Dieses Werk ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/ch/">Creative Commons Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Schweiz Lizenz</a>.

- - - 
